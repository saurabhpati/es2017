/**
 * This file introduces the concept of promises in ES 2015. Skip this in case you already know about this.
 */

let resolver = (result) => { console.log(result) },
    rejecter = (error) => { console.log(error) },
    callback = (resolve, reject) => {
            // In case this function throws an exception, rejecter will be called, otherwise resolver will be called.
            try {
                //throw 'Uncomment this line to call the rejector callback';
                resolve('resolved');
            } catch (error) {
                reject('rejected');
            }
    },
    promise = new Promise(callback);

promise
    .then(resolver)
    .catch(rejecter);
