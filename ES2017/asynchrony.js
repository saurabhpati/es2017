/**
 * This file depicts the usage of async and await keywords introduced in ES 2017.
 * Run this file by running the home.html in the browser and viewing the console window.
 */

// If a function is marked async, it can be awaited, although a promise has been returned, it wraps the result
// of the function in a promise itself.
let asyncFn = async (method, url) => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();

        xhr.onerror = (error) => {
            reject(error);
        };

        xhr.onload = () => {
            if (xhr.status >= 200 && xhr.status <= 304) {
                resolve(xhr.response);
            } else {
                reject({ status: xhr.status, statusText: xhr.statusText });
            }
        };

        xhr.open(method, url);
        xhr.send();
    });
}

let getMe = async () => {
    // The commented code would produce the same result.

    //let user = asyncFn('GET', 'https://api.github.com/users/saurabhpati')
    let user = await asyncFn('GET', 'https://api.github.com/users/saurabhpati');
    console.log(user);
    
    // user
    //     .then((user) => {
    //         console.log(user);
    //     })
    //     .catch((error) => {
    //         console.log(error);
    //     });
}

getMe();
