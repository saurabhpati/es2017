/**
 * explains the exponential operators that is included in ES 2016.
 */

console.log(Math.pow(3, 4)) // 3 to the power of 4, done in ES 5 way.
console.log(3 ** 4) // 3 to the power of 4, done in the ES 2016 way.