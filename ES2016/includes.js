/**
 * This file explains the new features of ES 2016 before diving into the new features of ES 2017
 */

// use includes function in the array prototype to find whether a particular element is in the array.

// 1. with strings 
let fruits = ['banana', 'mango', 'orange', 'apple'];

console.log(fruits.includes('watermelons')); // false
console.log(fruits.includes('orange')); //true

// 2. with integers
let numbers = [2, 3, 5, NaN, 7];
console.log(numbers.indexOf(5) >= 0); // true
console.log(numbers.indexOf(NaN) >= 0); // false
console.log(numbers.includes(3)); //true
console.log(numbers.includes('3')); //false

// 3. with objects, it will check for the object with the same reference.
let brocoli = {
    type: 'vegetable',
    name: 'brocoli'
};

let veggies = [{
    type: 'fruit',
    name: 'apple'
}, {
    type: 'fruit',
    name: 'mango'
}, brocoli];

console.log(veggies.includes(brocoli));
console.log(veggies.includes({
    type: 'vegetable',
    name: 'brocoli'
}));
